﻿namespace IKnow.BusinessLogic.Services
{
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class QuestionService : IQuestionService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public QuestionService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create new question
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public QuestionDto Create(QuestionDto question)
        {
            var newQuestion = _mapper.Map<QuestionDto, Question>(question);
            _database.Questions.Create(newQuestion);
            _database.Save();

            return _mapper.Map<Question, QuestionDto>(newQuestion);
        }

        /// <summary>
        /// Update question
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public QuestionDto Update(QuestionDto question)
        {
            var newQuestion = _mapper.Map<QuestionDto, Question>(question);
            _database.Questions.Update(newQuestion);
            _database.Save();

            return _mapper.Map<Question, QuestionDto>(newQuestion);
        }

        /// <summary>
        /// Get question by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QuestionDto GetById(long id)
        {
            return _mapper.Map<Question, QuestionDto>(_database.Questions.Get(id));
        }

        /// <summary>
        /// Soft delete question
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            _database.Questions.Delete(id);
            _database.Save();
        }
    }
}