﻿namespace IKnow.BusinessLogic.Services
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class ImageService : IImageService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public ImageService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new image record in and saves database
        /// </summary>
        /// <param name="newImage"></param>
        /// <returns></returns>
        public async Task<ImageDto> CreateAsync(ImageDto newImage)
        {
            var image = _mapper.Map<ImageDto, Image>(newImage);
            _database.Images.Create(image);
            await _database.SaveAsync();

            return _mapper.Map<Image, ImageDto>(image);
        }

        /// <summary>
        /// Get image by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ImageDto> GetByIdAsync(int id)
        {
            return _mapper.Map<Image, ImageDto>(await _database.Images.GetAsync(id));
        }

        /// <summary>
        /// Get image by path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<ImageDto> GetByPathAsync(string path)
        {
            return _mapper.Map<Image, ImageDto>(_database.Images.Find(i => i.Path == path).FirstOrDefault());
        }
    }
}