﻿namespace IKnow.BusinessLogic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class TestService : ITestService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public TestService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Create new test
        /// </summary>
        /// <param name="newTest"></param>
        /// <returns></returns>
        public async Task<TestDto> CreateAsync(TestDto newTest)
        {
            var testInfo = _mapper.Map<TestDto, Test>(newTest);
            _database.Tests.Create(testInfo);
            await _database.SaveAsync();

            return _mapper.Map<Test, TestDto>(testInfo);
        }

        /// <summary>
        /// Get test by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TestDto> GetByIdAsync(long id)
        {
            var test = await _database.Tests.GetAsync(id);

            return _mapper.Map<Test, TestDto>(test);
        }

        /// <summary>
        /// Get test by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TestDto GetById(long id)
        {
            var test = _database.Tests.Get(id);

            return _mapper.Map<Test, TestDto>(test);
        }

        /// <summary>
        /// Get all tests
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TestDto> GetAll()
        {
            return _mapper.Map<IEnumerable<Test>, IEnumerable<TestDto>>(_database.Tests.GetAll());
        }
        
        /// <summary>
        /// Get all tests with CategoryId
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<TestDto> GetAllByCategory(int categoryId)
        {
            return _mapper
                .Map<IEnumerable<Test>, IEnumerable<TestDto>
                >(_database.Tests.Find(t => t.Category.Id == categoryId));
        }
        

        /// <summary>
        /// Update test
        /// </summary>
        /// <param name="test"></param>
        /// <returns></returns>
        public async Task UpdateTestAsync(TestDto test)
        {
            _database.Tests.Update(_mapper.Map<TestDto, Test>(test));
            await _database.SaveAsync();
        }

        /// <summary>
        /// Search tests that match query in name or description
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<TestDto> GetByQuery(string query)
        {
            return _mapper.Map<IEnumerable<Test>, IEnumerable<TestDto>>(_database.Tests.Find(test =>
                                                                            test.Description != null &&
                                                                            test.Description
                                                                                .IndexOf(query,
                                                                                 StringComparison
                                                                                     .OrdinalIgnoreCase) !=
                                                                            -1
                                                                            || test.Name != null &&
                                                                            test.Name.IndexOf(query,
                                                                             StringComparison
                                                                                 .OrdinalIgnoreCase) !=
                                                                            -1));
        }

        /// <summary>
        /// Soft delete test
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            _database.Tests.Delete(id);
            _database.Save();
        }
    }
}