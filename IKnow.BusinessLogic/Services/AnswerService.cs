﻿namespace IKnow.BusinessLogic.Services
{
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class AnswerService : IAnswerService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public AnswerService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create answer
        /// </summary>
        /// <param name="questionAnswer"></param>
        /// <returns></returns>
        public AnswerDto Create(AnswerDto questionAnswer)
        {
            var newQuestionAnswer = _mapper.Map<AnswerDto, Answer>(questionAnswer);
            _database.Answers.Create(newQuestionAnswer);
            _database.Save();

            return _mapper.Map<Answer, AnswerDto>(newQuestionAnswer);
        }

        /// <summary>
        /// Update answer
        /// </summary>
        /// <param name="questionAnswer"></param>
        /// <returns></returns>
        public AnswerDto Update(AnswerDto questionAnswer)
        {
            var newQuestionAnswer = _mapper.Map<AnswerDto, Answer>(questionAnswer);
            _database.Answers.Update(newQuestionAnswer);
            _database.Save();

            return _mapper.Map<Answer, AnswerDto>(newQuestionAnswer);
        }

        /// <summary>
        /// Get question by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AnswerDto GetById(long id)
        {
            return _mapper.Map<Answer, AnswerDto>(_database.Answers.Get(id));
        }

        /// <summary>
        /// Soft delete
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            _database.Answers.Delete(id);
            _database.Save();
        }
    }
}