﻿namespace IKnow.BusinessLogic.Services
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class ProgressService : IProgressService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public ProgressService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Creates new record about started test
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="testId"></param>
        /// <returns></returns>
        public ProgressDto Start(long userId, long testId)
        {
            var progressDto = new ProgressDto {TestId = testId, StartTime = DateTime.Now, ProfileId = userId};
            var newProgressModel = _mapper.Map<ProgressDto, Progress>(progressDto);
            _database.Progresses.Create(newProgressModel);
            _database.Save();

            return _mapper.Map<Progress, ProgressDto>(newProgressModel);
        }

        /// <summary>
        /// Add answered question to progress record
        /// </summary>
        /// <param name="answer"></param>
        public void Answer(AnsweredQuestionDto answer)
        {
            _database.AnsweredQuestions.Create(_mapper.Map<AnsweredQuestionDto, AnsweredQuestion>(answer));
            _database.Save();
        }

        /// <summary>
        /// Get all started tests for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>

        public IEnumerable<ProgressDto> GetStarted(long userId)
        {
            return _mapper
                .Map<IEnumerable<Progress>, IEnumerable<ProgressDto>
                >(_database.Progresses.Find(p => p.ProfileId == userId && !p.Finished));
        }

        /// <summary>
        /// Get all tests for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<ProgressDto> GetAll(long userId)
        {
            return _mapper
                .Map<IEnumerable<Progress>, IEnumerable<ProgressDto>
                >(_database.Progresses.Find(p => p.ProfileId == userId));
        }

        /// <summary>
        /// Updates progress
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public ProgressDto Update(ProgressDto progress)
        {
            var mapped = _mapper.Map<ProgressDto, Progress>(progress);
            _database.Progresses.Update(mapped);
            _database.Save();

            return _mapper.Map<Progress, ProgressDto>(mapped);
        }

        /// <summary>
        /// Get all finished instances for test
        /// </summary>
        /// <param name="testId"></param>
        /// <returns></returns>
        public IEnumerable<ProgressDto> GetFinished(long testId)
        {
            return _mapper
                .Map<IEnumerable<Progress>, IEnumerable<ProgressDto>
                >(_database.Progresses.Find(t => t.TestId == testId && t.Finished));
        }

        /// <summary>
        /// Get progress by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProgressDto GetById(long id)
        {
            return _mapper.Map<Progress, ProgressDto>(_database.Progresses.Get(id));
        }

        /// <summary>
        /// Soft delete progress record
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            _database.Progresses.Delete(id);
            _database.Save();
        }
    }
}