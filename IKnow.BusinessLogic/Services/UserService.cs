﻿namespace IKnow.BusinessLogic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Infrastructure;
    using Interfaces;
    using Microsoft.AspNet.Identity;

    public class UserService : IUserService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new user instance in database.
        /// </summary>
        /// <exception cref="ServiceValidationException">Thrown when user with that email is already exists</exception>
        /// <param name="userDto">Should contain new user email and password</param>
        /// <returns></returns>
        public async Task<UserDto> RegisterUserAsync(UserDto userDto)
        {
            var newUser = await _database.IdentityUserManager.FindByEmailAsync(userDto.Email);
            if (newUser != null) throw new ServiceValidationException("Email", "User with such email already exists");

            newUser = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
            var result = await _database.IdentityUserManager.CreateAsync(newUser, userDto.Password);
            if (result.Errors.Any())
                throw new ServiceValidationException("", result.Errors.FirstOrDefault());

            await _database.IdentityUserManager.AddToRoleAsync(newUser.Id, userDto.Role);

            var userProfile = new UserProfile { ApplicationUserId = newUser.Id, RegisterTime = DateTime.Now };
            _database.Users.Create(userProfile);
            await _database.SaveAsync();

            newUser.UserProfileId = userProfile.Id;

            return _mapper.Map<UserProfile, UserDto>(userProfile);
        }

        /// <summary>
        /// Authorize user by <see cref="UserDto.Email" /> and <see cref="UserDto.Password" />
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public async Task<ClaimsIdentity> LogInAsync(UserDto userDto)
        {
            ClaimsIdentity claim = null;

            var user = await _database.IdentityUserManager.FindAsync(userDto.Email, userDto.Password);

            if (user != null)
                claim = await _database.IdentityUserManager.CreateIdentityAsync(user,
                                                                                DefaultAuthenticationTypes
                                                                                    .ApplicationCookie);
            return claim;
        }

        /// <summary>
        /// Get user profile by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<UserDto> GetByEmailAsync(string email)
        {
            var appUser = await _database.IdentityUserManager.FindByEmailAsync(email);
            if (appUser == null) return null;

            var userProfile = _database.Users.Find(profile => profile.ApplicationUser.Id == appUser.Id)
                                       .FirstOrDefault();

            return _mapper.Map<UserProfile, UserDto>(userProfile);
        }

        /// <summary>
        /// Get user profile by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserDto GetByEmail(string email)
        {
            var appUser = _database.IdentityUserManager.FindByEmail(email);
            if (appUser == null) return null;

            var userProfile = _database.Users.Find(profile => profile.ApplicationUser.Id == appUser.Id)
                                       .FirstOrDefault();

            return _mapper.Map<UserProfile, UserDto>(userProfile);
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserDto>>(_database.Users.GetAll());
        }

        public void Update(UserDto map)
        {
            _database.Users.Update(_mapper.Map<UserDto, UserProfile>(map));
            _database.Save();
        }

        public void Delete(long id)
        { 
            _database.Users.Delete(id);
            _database.Save();
        }

        public async Task<UserDto> GetByIdAsync(long id)
        {
            var user = await _database.Users.GetAsync(id);
            return _mapper.Map<UserProfile, UserDto>(user);
        }
    }
}