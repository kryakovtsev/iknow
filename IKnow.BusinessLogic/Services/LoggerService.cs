﻿namespace IKnow.BusinessLogic.Services
{
    using System.Collections.Generic;
    using System.Runtime.InteropServices.ComTypes;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class LoggerService : ILoggerService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public LoggerService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Add log record to database with Warn level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stackTrace"></param>
        public void Warn(string message, string stackTrace = null)
        {
            _database.Logger.Create(new LoggerRecord {Message = message, StackTrace = stackTrace, Level = "Warn"});
            _database.Save();
        }

        /// <summary>
        /// Add log record to database with Error level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stackTrace"></param>

        public void Error(string message, string stackTrace = null)
        {
            _database.Logger.Create(new LoggerRecord {Message = message, StackTrace = stackTrace, Level = "Error"});
            _database.Save();
        }

        /// <summary>
        /// Add log record to database with Info level
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stackTrace"></param>
        public void Info(string message, string stackTrace = null)
        {
            _database.Logger.Create(new LoggerRecord {Message = message, StackTrace = stackTrace, Level = "Info"});
            _database.Save();
        }
    }
}