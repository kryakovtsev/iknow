﻿namespace IKnow.BusinessLogic.Services
{
    using System.Collections.Generic;
    using AutoMapper;
    using DataAccess.DataContext;
    using DataAccess.Entities;
    using DataTransferObjects;
    using Interfaces;

    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork database, IMapper mapper)
        {
            _database = database;
            _mapper = mapper;
        }

        /// <summary>
        /// Create new category and saves database
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public CategoryDto Create(CategoryDto category)
        {
            var mapped = _mapper.Map<CategoryDto, Category>(category);
            _database.Categories.Create(mapped);
            _database.Save();
            return _mapper.Map<Category, CategoryDto>(mapped);
        }

        /// <summary>
        /// Query all categories that not deleted
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CategoryDto> GetAll()
        {
            return _mapper
                .Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(_database.Categories.GetAll());
        }

        /// <summary>
        /// Updates category and saves database
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public CategoryDto Update(CategoryDto category)
        {
            var mapped = _mapper.Map<CategoryDto, Category>(category);
            _database.Categories.Update(mapped);
            _database.Save();
            return _mapper.Map<Category, CategoryDto>(mapped);
        }

        /// <summary>
        /// Get category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CategoryDto GetById(long id)
        {
            return _mapper.Map<Category, CategoryDto>(_database.Categories.Get(id));
        }

        /// <summary>
        /// Performs soft delete of category
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            _database.Categories.Delete(id);
            _database.Save();
        }
    }
}