﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    using System.Collections.Generic;
    using System.Linq;

    public class QuestionDto
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public List<AnswerDto> Answers { get; set; }
        public TestDto Test { get; set; }
        public long? TestId { get; set; }
        public bool Deleted { get; set; }

        public List<AnswerDto> GetAvailableAnswers()
        {
            return Answers.Where(a => !a.Deleted).ToList();
        }
    }
}