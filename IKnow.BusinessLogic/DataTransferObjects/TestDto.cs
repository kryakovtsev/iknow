﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TestDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexBackground { get; set; }
        public int TimeLimit { get; set; }
        public int Views { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EditTime { get; set; }

        public ImageDto Image { get; set; }
        public long? ImageId { get; set; }
        public CategoryDto Category { get; set; }
        public long? CategoryId { get; set; }

        public List<QuestionDto> Questions { get; set; }

        public List<QuestionDto> GetAvailableQuestions()
        {
            return Questions.Where(q => !q.Deleted).ToList();
        }
    }
}