﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    public class AnsweredQuestionDto
    {
        public long Id { get; set; }
        public long? QuestionId { get; set; }
        public virtual QuestionDto Question { get; set; }
        public long? AnswerId { get; set; }
        public virtual AnswerDto Answer { get; set; }
        public long? ProgressId { get; set; }
    }
}