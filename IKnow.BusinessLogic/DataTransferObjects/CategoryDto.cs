﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    public class CategoryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}