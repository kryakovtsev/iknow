﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    using System;
    using System.Collections.Generic;

    public class ProgressDto
    {
        public long Id { get; set; }
        public virtual UserDto Profile { get; set; }
        public long? ProfileId { get; set; }
        public virtual TestDto Test { get; set; }
        public long? TestId { get; set; }

        public virtual List<AnsweredQuestionDto> Answered { get; set; }

        public bool Finished { get; set; }
        public DateTime StartTime { get; set; }

        public bool IsTimeOver()
        {
            if (Test.TimeLimit == 0) return false;

            return StartTime.AddSeconds(Test.TimeLimit) < DateTime.Now;
        }
    }
}