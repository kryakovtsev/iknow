﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    public class ImageDto
    {
        public long Id { get; set; }
        public string Path { get; set; }
    }
}