﻿namespace IKnow.BusinessLogic.DataTransferObjects
{
    public class LoggerRecordDto
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Level { get; set; }
    }
}