﻿namespace IKnow.BusinessLogic.Infrastructure
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ServiceValidationException : Exception
    {
        public ServiceValidationException(string prop, string message) : base(message)
        {
            Property = prop;
        }

        protected ServiceValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public string Property { get; protected set; }
    }
}