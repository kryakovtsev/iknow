﻿namespace IKnow.BusinessLogic.Infrastructure
{
    using DataAccess.DataContext;
    using Ninject.Modules;

    public class UnitOfWorkModule : NinjectModule
    {
        private readonly string _connectionString;

        public UnitOfWorkModule(string connection)
        {
            _connectionString = connection;
        }

        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(_connectionString);
        }
    }
}