﻿namespace IKnow.BusinessLogic.Infrastructure
{
    using System.Linq;
    using AutoMapper;
    using DataAccess.Entities;
    using DataTransferObjects;

    public class ModelToDtoProfile : Profile
    {
        public ModelToDtoProfile()
        {
            CreateMap<Image, ImageDto>();
            CreateMap<UserProfile, UserDto>()
                .ForMember(d => d.Email, o => o.MapFrom(s => s.ApplicationUser.Email))
                .ForMember(d => d.Password, o => o.MapFrom(s => s.ApplicationUser.PasswordHash));
            CreateMap<Answer, AnswerDto>();
            CreateMap<Question, QuestionDto>();
            CreateMap<Category, CategoryDto>();
            CreateMap<Test, TestDto>();
            CreateMap<AnsweredQuestion, AnsweredQuestionDto>();
            CreateMap<Progress, ProgressDto>();
            CreateMap<LoggerRecord, LoggerRecordDto>();

            CreateMap<ImageDto, Image>();
            CreateMap<UserDto, UserProfile>()
                .ForPath(d => d.ApplicationUser.Email, o => o.MapFrom(s => s.Email))
                .ForPath(d => d.ApplicationUser.PasswordHash, o => o.MapFrom(s => s.Password));
            CreateMap<AnswerDto, Answer>();
            CreateMap<QuestionDto, Question>();
            CreateMap<CategoryDto, Category>();
            CreateMap<TestDto, Test>();
            CreateMap<AnsweredQuestionDto, AnsweredQuestion>();
            CreateMap<ProgressDto, Progress>();
        }
    }
}