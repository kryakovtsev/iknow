﻿namespace IKnow.BusinessLogic.Interfaces
{
    using System.Collections.Generic;
    using DataTransferObjects;

    public interface IProgressService
    {
        ProgressDto Start(long userId, long testId);
        void Answer(AnsweredQuestionDto answer);
        IEnumerable<ProgressDto> GetStarted(long userId);
        IEnumerable<ProgressDto> GetAll(long userId);
        ProgressDto Update(ProgressDto progress);
        IEnumerable<ProgressDto> GetFinished(long testId);
        ProgressDto GetById(long id);
        void Delete(long id);
    }
}