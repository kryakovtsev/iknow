﻿namespace IKnow.BusinessLogic.Interfaces
{
    public interface ILoggerService
    {
        void Warn(string message, string stackTrace = null);
        void Error(string message, string stackTrace = null);
        void Info(string message, string stackTrace = null);
    }
}