﻿namespace IKnow.BusinessLogic.Interfaces
{
    using System.Collections.Generic;
    using DataTransferObjects;

    public interface ICategoryService
    {
        CategoryDto Create(CategoryDto category);
        IEnumerable<CategoryDto> GetAll();
        CategoryDto Update(CategoryDto category);
        CategoryDto GetById(long id);
        void Delete(long id);
    }
}