﻿namespace IKnow.BusinessLogic.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using DataTransferObjects;

    public interface IImageService
    {
        Task<ImageDto> CreateAsync(ImageDto newImage);
        Task<ImageDto> GetByIdAsync(int id);
        Task<ImageDto> GetByPathAsync(string path);
    }
}