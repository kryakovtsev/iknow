﻿namespace IKnow.BusinessLogic.Interfaces
{
    using DataTransferObjects;

    public interface IAnswerService
    {
        AnswerDto Create(AnswerDto questionAnswer);
        AnswerDto Update(AnswerDto questionAnswer);
        AnswerDto GetById(long id);
        void Delete(long id);
    }
}