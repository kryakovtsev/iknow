﻿namespace IKnow.BusinessLogic.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataTransferObjects;

    public interface ITestService
    {
        Task<TestDto> CreateAsync(TestDto newTest);
        Task<TestDto> GetByIdAsync(long id);
        TestDto GetById(long id);
        IEnumerable<TestDto> GetAll();
        IEnumerable<TestDto> GetAllByCategory(int categoryId);
        Task UpdateTestAsync(TestDto test);
        IEnumerable<TestDto> GetByQuery(string query);
        void Delete(long id);
    }
}