﻿namespace IKnow.BusinessLogic.Interfaces
{
    using DataTransferObjects;

    public interface IQuestionService
    {
        QuestionDto Create(QuestionDto question);
        QuestionDto Update(QuestionDto question);
        QuestionDto GetById(long id);
        void Delete(long id);
    }
}