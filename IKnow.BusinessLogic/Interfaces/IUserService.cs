﻿namespace IKnow.BusinessLogic.Interfaces
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using DataTransferObjects;

    public interface IUserService
    {
        Task<UserDto> RegisterUserAsync(UserDto userDto);
        Task<ClaimsIdentity> LogInAsync(UserDto userDto);
        Task<UserDto> GetByEmailAsync(string email);
        UserDto GetByEmail(string email);

        IEnumerable<UserDto> GetAll();
        void Update(UserDto map);
        void Delete(long id);
        Task<UserDto> GetByIdAsync(long id);
    }
}