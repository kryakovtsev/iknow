﻿using IKnow.Web;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace IKnow.Web
{
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security.Cookies;
    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
                                        {
                                            AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                                            LoginPath = new PathString("/auth/login")
                                        });
        }
    }
}