﻿namespace IKnow.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Microsoft.Owin.Logging;
    using Web.Models;

    [Authorize(Roles = "admin")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;
        private ILoggerService _logger;

        public CategoryController(ICategoryService categoryService, IMapper mapper, ILoggerService logger)
        {
            _categoryService = categoryService;
            _mapper = mapper;
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            var newCategory = new CategoryDto {Name = "New category"};

            return PartialView("_Create", _mapper.Map<CategoryDto, CategoryViewModel>(newCategory));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel category)
        {
            if (!ModelState.IsValid) return PartialView("_Create", category);

            try
            {
                _categoryService.Create(_mapper.Map<CategoryViewModel, CategoryDto>(category));
            }
            catch(Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new { success = false });
            }

            var url = Url.Action("List", "Category");
            return Json(new {success = true, url});
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var category = _categoryService.GetById((long) id);
            if (category == null) 
                return HttpNotFound();

            return PartialView("_Edit", _mapper.Map<CategoryDto, CategoryViewModel>(category));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryViewModel category)
        {
            if (!ModelState.IsValid) return PartialView("_Edit", category);

            try
            {
                _categoryService
                    .Update(_mapper.Map<CategoryViewModel, CategoryDto>(category));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new { success = false });
            }

            var url = Url.Action("List", "Category");
            return Json(new {success = true, url});
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var category = _categoryService.GetById((long) id);
            if (category == null) 
                return HttpNotFound();

            return PartialView("_Delete", _mapper.Map<CategoryDto, CategoryViewModel>(category));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _categoryService
                    .Delete(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new { success = false });
            }

            var url = Url.Action("List", "Category");
            return Json(new {success = true, url});
        }

        public ActionResult List()
        {
            return PartialView("_List",
                               _mapper.Map<IEnumerable<CategoryDto>, IEnumerable<CategoryViewModel>>(_categoryService
                                   .GetAll()));
        }
    }
}