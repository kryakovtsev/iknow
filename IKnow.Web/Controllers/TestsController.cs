﻿namespace IKnow.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Infrastructure;
    using Models;

    public class TestsController : Controller
    {
        private readonly IImageService _imageService;
        private readonly IMapper _mapper;
        private readonly Random _random;
        private readonly IProgressService _progress;
        private readonly ITestService _testService;
        private readonly IUserService _userService;
        private readonly ICategoryService _categoryService;
        private bool _disposed;
        private readonly ILoggerService _logger;

        public TestsController(ITestService testService, IImageService imageService, IProgressService progress,
                               IUserService userService, ICategoryService categoryService, IMapper mapper, ILoggerService logger)
        {
            _random = new Random();
            _testService = testService;
            _imageService = imageService;
            _progress = progress;
            _userService = userService;
            _categoryService = categoryService;
            _mapper = mapper;
            _logger = logger;

            Categories = _mapper.Map<
                IEnumerable<CategoryDto>,
                IEnumerable<CategoryViewModel>
            >(_categoryService.GetAll());

            FilterTypes = new List<TestListFilterHelper.FilterType>
                          {
                              new TestListFilterHelper.FilterType(0, "All", test => true),
                              new TestListFilterHelper.FilterType(1, "Up to 10 min", test => test.TimeLimit <= 60 * 10),
                              new TestListFilterHelper.FilterType(2, "From 10 min to 20 min",
                                                                  test => test.TimeLimit > 60 * 10 &&
                                                                          test.TimeLimit <= 60 * 20),
                              new TestListFilterHelper.FilterType(3, "From 20 min", test => test.TimeLimit > 60 * 20),
                              new TestListFilterHelper.FilterType(4, "Finished", test =>
                                                                  {
                                                                      var allProgresses =
                                                                          _progress.GetAll(_userService
                                                                              .GetByEmail(User
                                                                                  .Identity.Name)
                                                                              .Id);

                                                                      return allProgresses.FirstOrDefault(t =>
                                                                                     t.TestId == test.Id
                                                                                 )?.Finished == true;
                                                                  })
                          };

            SortTypes = new List<TestListFilterHelper.SortType>
                        {
                            new TestListFilterHelper.SortType(0, "Popular",
                                                              (test1, test2) => test2.Views.CompareTo(test1.Views)),
                            new TestListFilterHelper.SortType(1, "New",
                                                              (test1, test2) =>
                                                                  (int) (test2.CreationTime - test1.CreationTime)
                                                                  .TotalMilliseconds),
                            new TestListFilterHelper.SortType(2, "Old",
                                                              (test1, test2) =>
                                                                  (int) (test1.CreationTime - test2.CreationTime)
                                                                  .TotalMilliseconds),
                            new TestListFilterHelper.SortType(3, "From A-Z",
                                                              (test1, test2) =>
                                                                  string.Compare(test1.Name, test2.Name,
                                                                   StringComparison.Ordinal)),
                            new TestListFilterHelper.SortType(4, "From Z-A",
                                                              (test1, test2) =>
                                                                  string.Compare(test2.Name, test1.Name,
                                                                   StringComparison.Ordinal))
                        };
        }

        private IEnumerable<CategoryViewModel> Categories { get; }
        private IEnumerable<TestListFilterHelper.FilterType> FilterTypes { get; }
        private IEnumerable<TestListFilterHelper.SortType> SortTypes { get; }

        public async Task<ActionResult> Index(TestListViewModel model)
        {
            model.CategoryTypes =
                model.CategoryTypes.Concat(Categories.Select(m => new SelectListItem
                                                                  {Text = m.Name, Value = m.Id.ToString()}));
            model.FilterTypes = FilterTypes.Select(f => (SelectListItem) f).Reverse()
                                           .Skip(User.Identity.IsAuthenticated ? 0 : 1).Reverse();
            model.SortTypes = SortTypes.Select(s => (SelectListItem) s);
            var helper = new TestListFilterHelper(FilterTypes, SortTypes);

            var allTests = string.IsNullOrWhiteSpace(model.Query)
                               ? _testService.GetAll()
                               : _testService.GetByQuery(model.Query);

            model.Tests = _mapper.Map<IEnumerable<TestDto>, IEnumerable<TestViewModel>>(
             helper.FilterAndSort(allTests, model.CategoryId, model.FilterTypeId, model.SortTypeId));

            return View(model);
        }

        public async Task<ActionResult> Open(int id)
        {
            var test = await _testService.GetByIdAsync(id);
            test.Views += 1;
            await _testService.UpdateTestAsync(test);
            var model = _mapper.Map<TestDto, TestViewModel>(test);

            var user = await _userService.GetByEmailAsync(User?.Identity?.Name);
            if (user == null) return View(model);

            var testProgress = _progress.GetAll(user.Id).FirstOrDefault(p => p.TestId == id);
            if (testProgress == null) return View(model);

            model.Continue = !testProgress.Finished;
            model.Finished = testProgress.Finished;
            if (testProgress.Finished)
                model.TestResult = testProgress.Answered.Count(a => a.Answer.IsCorrect);

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(int id)
        {
            var test = await _testService.GetByIdAsync(id);
            ViewBag.Categories = Categories.Select(m => new SelectListItem
                                                        {Text = m.Name, Value = m.Id.ToString()});

            return View(_mapper.Map<TestDto, TestViewModel>(test));
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(TestViewModel test, HttpPostedFileBase preview)
        {
            if (!ModelState.IsValid)
                return View(test);

            if (preview != null && preview.ContentLength > 0)
            {
                try
                {
                    var randomBytes = new byte[8];
                    _random.NextBytes(randomBytes);

                    Directory.CreateDirectory(Server.MapPath("/Content/Uploaded"));

                    var path = Path.Combine("/Content/Uploaded",
                                            DateTimeOffset.Now.ToUnixTimeMilliseconds()
                                            + BitConverter.ToString(randomBytes).Replace("-", "") + "." +
                                            preview.FileName.Split('.').LastOrDefault());

                    preview.SaveAs(Server.MapPath(path));

                    test.ImageId = (await _imageService.CreateAsync(new ImageDto {Path = path})).Id;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message, ex.StackTrace);
                    return new HttpStatusCodeResult(500);
                }
            }
            else
            {
                var currentTestData = await _testService.GetByIdAsync(test.Id);
                test.ImageId = currentTestData.ImageId;
            }

            var testDto = _mapper.Map<TestViewModel, TestDto>(test);
            await _testService.UpdateTestAsync(testDto);

            return RedirectToAction("Open", new {testDto.Id});
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create()
        {
            var test = new TestDto {Name = "Sample name", CreationTime = DateTime.Now, EditTime = DateTime.Now};

            var created = await _testService.CreateAsync(test);

            return RedirectToAction("Edit", new {id = created.Id});
        }
        
        [Authorize(Roles = "admin")]
        public ActionResult Delete(long? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var test = _testService.GetById((long) id);
            if (test == null) 
                return HttpNotFound();

            return PartialView("_Delete", _mapper.Map<TestDto, TestViewModel>(test));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(long id)
        {
            _testService.Delete(id);

            var url = Url.Action("Index", "Tests");
            return Json(new {success = true, url});
        }
    }
}