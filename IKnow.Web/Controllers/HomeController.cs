﻿namespace IKnow.Web.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;

    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<ActionResult> Index()
        {
            if (await _userService.GetByEmailAsync(User?.Identity?.Name) != null)
                return RedirectToAction("Index", "Profile");

            return View();
        }
    }
}