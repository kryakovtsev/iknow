﻿namespace IKnow.Controllers
{
    using System.Web.Mvc;
    using Web.Models;

    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult NotFound()
        {
            var error = new ErrorViewModel
                        {
                            Code = "404",
                            Message = "The page you are requesting is not found or was deleted"
                        };
            return View("Error", error);
        }

        public ActionResult InternalError()
        {
            var error = new ErrorViewModel
                        {
                            Code = "500",
                            Message = "The page you are requesting is not found"
                        };
            return View("Error", error);
        }
    }
}