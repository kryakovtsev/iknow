﻿namespace IKnow.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Web.Models;

    public class ProgressController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IProgressService _progressService;
        private readonly IQuestionService _questionService;
        private readonly ITestService _testService;
        private readonly IUserService _userService;

        public ProgressController(ITestService testService, IQuestionService questionService,
                                  IProgressService progressService, IUserService userService,
                                  IMapper mapper)
        {
            _testService = testService;
            _questionService = questionService;
            _progressService = progressService;
            _userService = userService;
            _mapper = mapper;
        }

        [Authorize]
        public async Task<ActionResult> Start(int id)
        {
            if (id <= 0)
                return null;

            var user = await _userService.GetByEmailAsync(User.Identity.Name);
            var startedTest = _progressService.GetStarted(user.Id).FirstOrDefault(p => p.TestId == id);
            if (startedTest != null) return await Current(id);

            _progressService.Start(user.Id, id);
            return await Current(id);
        }

        [Authorize]
        public async Task<ActionResult> Answer(int testId, int id)
        {
            var user = await _userService.GetByEmailAsync(User.Identity.Name);
            var startedTest = _progressService.GetStarted(user.Id).FirstOrDefault(t => t.TestId == testId);
            if (startedTest == null) return null;
            var test = await _testService.GetByIdAsync((long) startedTest.TestId);

            var notAnswered = test.GetAvailableQuestions()
                                  .Where(q => startedTest.Answered.FirstOrDefault(a => a.QuestionId == q.Id) == null)
                                  .ToList();
            if (!notAnswered.Any() || startedTest.IsTimeOver())
                return await Done(startedTest);

            var answered = new AnsweredQuestionDto
                           {
                               ProgressId = startedTest.Id,
                               QuestionId = notAnswered.First().Id,
                               AnswerId = id
                           };

            _progressService.Answer(answered);
            return await Current(testId);
        }

        private async Task<ActionResult> Current(int testId)
        {
            var user = await _userService.GetByEmailAsync(User.Identity.Name);
            var startedTest = _progressService.GetStarted(user.Id).FirstOrDefault(t => t.TestId == testId);
            if (startedTest == null) return null;

            var test = await _testService.GetByIdAsync((long) startedTest.TestId);

            var notAnswered = test.GetAvailableQuestions()
                                  .Where(q => startedTest.Answered.FirstOrDefault(a => a.QuestionId == q.Id) == null)
                                  .ToList();
            if (!notAnswered.Any() || startedTest.IsTimeOver())
                return await Done(startedTest);
            var model = _mapper
                .Map<QuestionDto, QuestionViewModel>(notAnswered.FirstOrDefault());
            model.IsLastQuestion = notAnswered.Count == 1;
            model.QuestionsCount = test.GetAvailableQuestions().Count();
            model.AnsweredQuestionsCount = startedTest.Answered.Count();
            model.EndTime = startedTest.Test.TimeLimit == 0
                                ? 0
                                : ((DateTimeOffset) startedTest.StartTime.AddSeconds(startedTest.Test.TimeLimit))
                                .ToUnixTimeMilliseconds();
            model.NowTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            return PartialView("_Question", model);
        }

        [Authorize]
        public async Task<ActionResult> Finish(int testId)
        {
            var user = await _userService.GetByEmailAsync(User.Identity.Name);
            var startedTest = _progressService.GetStarted(user.Id).FirstOrDefault(t => t.TestId == testId);
            if (startedTest == null) return RedirectToAction("Open", "Tests", new {id = testId});

            return await Done(startedTest);
        }


        private async Task<ActionResult> Done(ProgressDto started)
        {
            if (started == null) return null;

            started.Finished = true;
            _progressService.Update(started);

            var test = await _testService.GetByIdAsync((long) started.TestId);

            ViewBag.QuestionsCount = test.GetAvailableQuestions().Count();
            ViewBag.CorrectAnsweredQuestions = started.Answered.Count(q => q.Answer.IsCorrect);

            return RedirectToAction("Open", "Tests", new {id = started.TestId});
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> AllResults(int testId)
        {
            ViewBag.TestId = testId;
            return View("Results");
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Details(int progressId)
        {
            var progress = _progressService.GetById(progressId);
            return View(_mapper.Map<ProgressDto, ProgressViewModel>(progress));
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var progress = _progressService.GetById((long) id);
            if (progress == null) return HttpNotFound();

            return PartialView("_Delete", _mapper.Map<ProgressDto, ProgressViewModel>(progress));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            var progress = _progressService.GetById(id);
            if (progress == null) return HttpNotFound();

            _progressService.Delete(progress.Id);

            var url = Url.Action("List", "Progress", new {testId = progress.TestId});
            return Json(new {success = true, url});
        }

        [Authorize(Roles = "admin")]
        public ActionResult List(int testId)
        {
            return PartialView("_List",
                               _mapper
                                   .Map<IEnumerable<ProgressDto>, IEnumerable<ProgressViewModel>
                                   >(_progressService.GetFinished(testId)));
        }
    }
}