﻿namespace IKnow.Web.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Models;

    [Authorize(Roles = "admin")]
    public class QuestionController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQuestionService _questionService;
        private readonly ITestService _testService;

        public QuestionController(ITestService testService, IQuestionService questionService,
                                  IMapper mapper)
        {
            _testService = testService;
            _questionService = questionService;
            _mapper = mapper;
        }

        public ActionResult Index(int id)
        {
            var test = _testService.GetById(id);
            ViewBag.TestId = test.Id;

            return PartialView("_Index",
                               _mapper
                                   .Map<IEnumerable<QuestionDto>, IEnumerable<QuestionViewModel>
                                   >(test.GetAvailableQuestions()));
        }

        public ActionResult List(int id)
        {
            var test = _testService.GetById(id);

            return PartialView("_List",
                               _mapper
                                   .Map<IEnumerable<QuestionDto>, IEnumerable<QuestionViewModel>
                                   >(test.GetAvailableQuestions()));
        }

        public ActionResult Create(int testId)
        {
            var testQuestion = new QuestionDto {Text = "Sample text", TestId = testId};

            return PartialView("_Create", _mapper.Map<QuestionDto, QuestionViewModel>(testQuestion));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionViewModel question)
        {
            if (ModelState.IsValid)
            {
                _questionService
                    .Create(_mapper.Map<QuestionViewModel, QuestionDto>(question));

                var url = Url.Action("Index", "Question", new {id = question.TestId});
                return Json(new {success = true, url});
            }

            return PartialView("_Create", question);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var testQuestion = _questionService.GetById((long) id);
            if (testQuestion == null) return HttpNotFound();

            return PartialView("_Edit", _mapper.Map<QuestionDto, QuestionViewModel>(testQuestion));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionViewModel question)
        {
            if (ModelState.IsValid)
            {
                _questionService
                    .Update(_mapper.Map<QuestionViewModel, QuestionDto>(question));

                var url = Url.Action("Index", "Question", new {id = question.TestId});
                return Json(new {success = true, url});
            }


            return PartialView("_Edit", question);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var questionDto = _questionService.GetById((long) id);
            if (questionDto == null) return HttpNotFound();
            return PartialView("_Delete", _mapper.Map<QuestionDto, QuestionViewModel>(questionDto));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var questionDto = _questionService.GetById(id);
            _questionService.Delete(id);

            var url = Url.Action("Index", "Question", new {id = questionDto.TestId});
            return Json(new {success = true, url});
        }
    }
}