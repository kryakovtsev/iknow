﻿namespace IKnow.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Microsoft.AspNet.Identity.Owin;
    using Models;

    public class ProfileController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IProgressService _progress;
        private readonly IUserService _userService;
        private readonly ILoggerService _logger;

        public ProfileController(IUserService userService, IProgressService progress, IMapper mapper, ILoggerService logger)
        {
            _userService = userService;
            _progress = progress;
            _mapper = mapper;
            _logger = logger;
        }

        [Authorize]
        public async Task<ActionResult> Index()
        {
            var user = await _userService.GetByEmailAsync(User.Identity.Name);
            var model = _mapper.Map<UserDto, UserViewModel>(user);
            var allProgresses =
                _progress.GetAll(user.Id);

            model.Tests = _mapper.Map<IEnumerable<TestDto>, IEnumerable<TestViewModel>>(
             allProgresses.Where(t => t.Finished).Select(t => t.Test));

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> GetUsers()
        {
            return View("List");
        }

        [Authorize(Roles = "admin")]
        public ActionResult List()
        {
            var users = _userService.GetAll();

            return PartialView("_UsersList",
                               _mapper.Map<IEnumerable<UserDto>, IEnumerable<UserViewModel>>(users));
        }

        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = await _userService.GetByIdAsync((long)id);
            if (user == null)
                return HttpNotFound();

            return PartialView("_Edit", _mapper.Map<UserDto, UserViewModel>(user));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel user)
        {
            if (!ModelState.IsValid) return PartialView("_Edit", user);

            try
            {
                _userService
                    .Update(_mapper.Map<UserViewModel, UserDto>(user));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new { success = false });
            }

            var url = Url.Action("List", "Profile");
            return Json(new { success = true, url });
        }

        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = await _userService.GetByIdAsync((long)id);
            if (user == null)
                return HttpNotFound();

            return PartialView("_Delete", _mapper.Map<UserDto, UserViewModel>(user));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            try
            {
                _userService
                    .Delete(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new { success = false });
            }

            var url = Url.Action("List", "Profile");
            return Json(new { success = true, url });
        }
    }
}