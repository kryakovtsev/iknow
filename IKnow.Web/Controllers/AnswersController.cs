﻿namespace IKnow.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Models;

    [Authorize(Roles = "admin")]
    public class AnswersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAnswerService _answerService;
        private readonly IQuestionService _questionService;
        private readonly ILoggerService _logger;

        public AnswersController(IAnswerService answerService,
                                 IQuestionService questionService, IMapper mapper, ILoggerService logger)
        {
            _answerService = answerService;
            _questionService = questionService;
            _mapper = mapper;
            _logger = logger;
        }

        public ActionResult Index(int id)
        {
            var question = _questionService.GetById(id);
            if (question == null)
                return HttpNotFound();

            ViewBag.TestQuestionId = question.Id;

            return PartialView("_Index",
                               _mapper
                                   .Map<IEnumerable<AnswerDto>, IEnumerable<AnswerViewModel>
                                   >(question.GetAvailableAnswers()));
        }

        public ActionResult Create(int testQuestionId)
        {
            var testQuestion = new AnswerDto {Text = "Sample text", QuestionId = testQuestionId};

            return PartialView("_Create",
                               _mapper.Map<AnswerDto, AnswerViewModel>(testQuestion));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AnswerViewModel questionAnswer)
        {
            if (!ModelState.IsValid) return PartialView("_Create", questionAnswer);

            try
            {
                _answerService.Create(_mapper
                                          .Map<AnswerViewModel, AnswerDto
                                          >(questionAnswer));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex.StackTrace);
                return Json(new {success = false});
            }


            var url = Url.Action("Index", "Answers", new {id = questionAnswer.QuestionId});
            return Json(new {success = true, url});

        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var testQuestion = _answerService.GetById((long) id);
            if (testQuestion == null) 
                return HttpNotFound();

            return PartialView("_Edit", _mapper.Map<AnswerDto, AnswerViewModel>(testQuestion));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AnswerViewModel questionAnswer)
        {
            if (!ModelState.IsValid) return PartialView("_Edit", questionAnswer);
            
            _answerService
                .Update(_mapper
                            .Map<AnswerViewModel, AnswerDto>(questionAnswer));

            var url = Url.Action("Index", "Answers", new {id = questionAnswer.QuestionId});
            return Json(new {success = true, url});

        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var answerDto = _answerService.GetById((long) id);
            if (answerDto == null) return HttpNotFound();

            return PartialView("_Delete", _mapper.Map<AnswerDto, AnswerViewModel>(answerDto));
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var answerDto = _answerService.GetById(id);
            _answerService.Delete(id);

            var url = Url.Action("Index", "Answers", new {id = answerDto.QuestionId});
            return Json(new {success = true, url});
        }
    }
}