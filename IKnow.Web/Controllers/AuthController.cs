﻿namespace IKnow.Web.Controllers
{
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Infrastructure;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Microsoft.Owin.Security;
    using Models;

    public class AuthController : Controller
    {
        private readonly ILoggerService _logger;
        private readonly IUserService _userService;

        public AuthController(IUserService userService, ILoggerService logger)
        {
            _userService = userService;
            _logger = logger;
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public ActionResult Index()
        {
            return RedirectToAction("LogIn");
        }

        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Profile");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AuthorizationViewModel auth, string returnUrl)
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Profile");
            if (!ModelState.IsValid) return View(auth);

            try
            {
                var userDto = new UserDto
                              {
                                  Email = auth.Email,
                                  Password = auth.Password,
                                  Role = "user"
                              };
                await _userService.RegisterUserAsync(userDto);
                var claim = await _userService.LogInAsync(userDto);
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                                             {
                                                 IsPersistent = true
                                             }, claim);

                _logger.Info($"New user is registered {claim.Name}");

                if (returnUrl != null)
                    return Redirect(returnUrl);
                return RedirectToAction("Index", "Profile");
            }
            catch (ServiceValidationException exception)
            {
                _logger.Error(exception.Message, exception.StackTrace);
                ModelState.AddModelError(exception.Property, exception.Message);
                return View(auth);
            }
        }

        public ActionResult LogIn()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Profile");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogIn(AuthorizationViewModel auth, string returnUrl)
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Profile");
            if (!ModelState.IsValid) return View(auth);

            var userDto = new UserDto {Email = auth.Email, Password = auth.Password};
            var claim = await _userService.LogInAsync(userDto);
            if (claim == null)
            {
                _logger.Warn($"Incorrect login was detected! User {userDto.Email}");
                ModelState.AddModelError("Password", "Password or email is not correct");
            }
            else
            {
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                                             {
                                                 IsPersistent = true
                                             }, claim);

                if (returnUrl != null)
                    return Redirect(returnUrl);

                return RedirectToAction("Index", "Profile");
            }

            return View(auth);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}