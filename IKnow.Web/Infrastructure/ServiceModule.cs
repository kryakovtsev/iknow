﻿namespace IKnow.Web.Infrastructure
{
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Ninject.Modules;

    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<ICategoryService>().To<CategoryService>();
            Bind<ITestService>().To<TestService>();
            Bind<IImageService>().To<ImageService>();
            Bind<IQuestionService>().To<QuestionService>();
            Bind<IAnswerService>().To<AnswerService>();
            Bind<IProgressService>().To<ProgressService>();
            Bind<ILoggerService>().To<LoggerService>();
        }
    }
}