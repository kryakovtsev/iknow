﻿namespace IKnow.Web.Infrastructure
{
    using AutoMapper;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Infrastructure;
    using Models;
    using Ninject.Modules;

    public class MapperModule : NinjectModule
    {
        public override void Load()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
                                                              {
                                                                  cfg.AddProfile<ModelToDtoProfile>();
                                                                  cfg.AddProfile<DtoToViewProfile>();
                                                              });
            Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();
        }
    }

    public class DtoToViewProfile : Profile
    {
        public DtoToViewProfile()
        {
            CreateMap<UserDto, UserViewModel>();
            CreateMap<ImageDto, ImageViewModel>();
            CreateMap<CategoryDto, CategoryViewModel>();
            CreateMap<QuestionDto, QuestionViewModel>();
            CreateMap<AnswerDto, AnswerViewModel>();
            CreateMap<TestDto, TestViewModel>();
            CreateMap<AnsweredQuestionDto, AnsweredQuestionViewModel>();
            CreateMap<ProgressDto, ProgressViewModel>();

            CreateMap<UserViewModel, UserDto>();
            CreateMap<ImageViewModel, ImageDto>();
            CreateMap<CategoryViewModel, CategoryDto>();
            CreateMap<QuestionViewModel, QuestionDto>();
            CreateMap<AnswerViewModel, AnswerDto>();
            CreateMap<TestViewModel, TestDto>();
            CreateMap<AnsweredQuestionViewModel, AnsweredQuestionDto>();
            CreateMap<ProgressViewModel, ProgressDto>();
        }
    }
}