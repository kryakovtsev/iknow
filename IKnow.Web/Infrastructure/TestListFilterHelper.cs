﻿namespace IKnow.Web.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using BusinessLogic.DataTransferObjects;

    public class TestListFilterHelper
    {
        private readonly IEnumerable<FilterType> _filters;
        private readonly IEnumerable<SortType> _sorts;

        public TestListFilterHelper(IEnumerable<FilterType> filters, IEnumerable<SortType> sorts)
        {
            _filters = filters;
            _sorts = sorts;
        }

        public IEnumerable<TestDto> FilterAndSort(IEnumerable<TestDto> tests, int categoryId, int filterId, int sortId)
        {
            var result = categoryId <= 0 ? tests : tests.Where(test => test?.Category?.Id == categoryId);
            var currentFilter = _filters.FirstOrDefault(f => f.Id == filterId);
            var currentSort = _sorts.FirstOrDefault(s => s.Id == sortId);

            if (currentFilter != null)
                result = result.Where(currentFilter.FilterFunc);
            if (currentSort != null)
                result = result.OrderBy(t => t, currentSort.Comparer);

            return result;
        }

        public class FilterType
        {
            public FilterType(int id, string name, Func<TestDto, bool> filterFunc)
            {
                Id = id;
                Name = name;
                FilterFunc = filterFunc;
            }

            public Func<TestDto, bool> FilterFunc { get; }
            public int Id { get; }
            public string Name { get; }

            public static implicit operator SelectListItem(FilterType f)
            {
                return new SelectListItem {Text = f.Name, Value = f.Id.ToString()};
            }
        }

        public class SortType
        {
            public SortType(int id, string name, Func<TestDto, TestDto, int> comparer)
            {
                Id = id;
                Name = name;
                Comparer = new SortTypeComparer(comparer);
            }

            public int Id { get; }
            public string Name { get; }
            public SortTypeComparer Comparer { get; }

            public static implicit operator SelectListItem(SortType s)
            {
                return new SelectListItem {Text = s.Name, Value = s.Id.ToString()};
            }

            public class SortTypeComparer : IComparer<TestDto>
            {
                private readonly Func<TestDto, TestDto, int> _compareFunc;

                public SortTypeComparer(Func<TestDto, TestDto, int> compareFunc)
                {
                    _compareFunc = compareFunc;
                }

                public int Compare(TestDto x, TestDto y)
                {
                    return _compareFunc(x, y);
                }
            }
        }
    }
}