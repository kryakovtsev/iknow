﻿namespace IKnow.Web.Models
{
    public class AnswerViewModel
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public bool Deleted { get; set; }
        public long? QuestionId { get; set; }
    }
}