﻿namespace IKnow.Web.Models
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class TestListViewModel
    {
        public IEnumerable<SelectListItem> SortTypes { get; set; } = new List<SelectListItem>();

        public IEnumerable<SelectListItem> FilterTypes { get; set; } = new List<SelectListItem>();

        public IEnumerable<SelectListItem> CategoryTypes { get; set; } = new List<SelectListItem>
                                                                         {
                                                                             new SelectListItem
                                                                             {Text = "All categories", Value = "0"}
                                                                         };

        public int SortTypeId { get; set; }
        public int FilterTypeId { get; set; }
        public int CategoryId { get; set; }

        public string Query { get; set; }
        public IEnumerable<TestViewModel> Tests { get; set; }
    }
}