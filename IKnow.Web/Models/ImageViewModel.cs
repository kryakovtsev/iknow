﻿namespace IKnow.Web.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public string Path { get; set; }
    }
}