﻿namespace IKnow.Web.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class QuestionViewModel
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public bool Deleted { get; set; }
        public List<AnswerViewModel> Answers { get; set; }
        public TestViewModel Test { get; set; }
        public long? TestId { get; set; }
        public int AnsweredQuestionsCount { get; set; }
        public int QuestionsCount { get; set; }
        public long EndTime { get; set; }
        public long NowTime { get; set; }
        public bool IsLastQuestion { get; set; }

        public List<AnswerViewModel> GetAvailableAnswers()
        {
            return Answers.Where(a => !a.Deleted).ToList();
        }
    }
}