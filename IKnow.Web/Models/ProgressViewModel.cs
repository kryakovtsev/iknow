﻿namespace IKnow.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ProgressViewModel
    {
        public long Id { get; set; }
        public virtual UserViewModel Profile { get; set; }
        public long? ProfileId { get; set; }
        public virtual TestViewModel Test { get; set; }
        public long? TestId { get; set; }

        public virtual List<AnsweredQuestionViewModel> Answered { get; set; }

        public bool Finished { get; set; }
        public DateTime StartTime { get; set; }

        public int GetCorrectAnswers()
        {
            return Answered.Select(a => a.Answer).Count(qa => qa.IsCorrect);
        }
    }
}