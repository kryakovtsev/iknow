﻿namespace IKnow.Web.Models
{
    using System;
    using System.Collections.Generic;

    public class UserViewModel
    {
        public long Id { get; set; }
        public DateTime RegisterTime { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public long? ImageId { get; set; }
        public ImageViewModel Image { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public string ApplicationUserId { get; set; }

        public bool Deleted { get; set; }

        public IEnumerable<TestViewModel> Tests { get; set; }
    }
}