﻿namespace IKnow.Web.Models
{
    public class AnsweredQuestionViewModel
    {
        public long Id { get; set; }
        public long? QuestionId { get; set; }
        public long? AnswerId { get; set; }
        public virtual AnswerViewModel Answer { get; set; }
        public virtual QuestionViewModel Question { get; set; }
        public long? ProgressId { get; set; }
    }
}