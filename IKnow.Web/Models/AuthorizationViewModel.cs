﻿namespace IKnow.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class AuthorizationViewModel
    {
        [Required]
        [RegularExpression("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$",
                              ErrorMessage = "Please provide a valid email.")]
        public string Email { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Password must be at least 8 characters.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}