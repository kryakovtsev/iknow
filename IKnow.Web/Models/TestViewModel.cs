﻿namespace IKnow.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class TestViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }

        [DataType(DataType.MultilineText)] public string Description { get; set; }

        public int TimeLimit { get; set; }
        public int Views { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EditTime { get; set; }

        [RegularExpression("^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$",
                              ErrorMessage = "Please provide a valid background color")]
        public string HexBackground { get; set; }

        public ImageViewModel Image { get; set; }
        public long? ImageId { get; set; }

        public long? CategoryId { get; set; }
        public CategoryViewModel Category { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
        public bool Deleted { get; set; }
        public bool Continue { get; set; }
        public bool Finished { get; set; }
        public int TestResult { get; set; }

        public List<QuestionViewModel> GetAvailableQuestions()
        {
            return Questions.Where(q => !q.Deleted).ToList();
        }
    }
}