﻿namespace IKnow.Web
{
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using BusinessLogic.Infrastructure;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using IKnow.Controllers;
    using Infrastructure;
    using Ninject;
    using Ninject.Modules;
    using Ninject.Web.Mvc;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule serviceModule = new ServiceModule();
            NinjectModule uowModule =
                new UnitOfWorkModule(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            NinjectModule mapper = new MapperModule();

            var kernel = new StandardKernel(serviceModule, uowModule, mapper);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            Response.Clear();

            var logger = DependencyResolver.Current.GetService(typeof(ILoggerService)) as ILoggerService;
            logger?.Error(exception.Message, exception.StackTrace);

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");

            if (!(exception is HttpException httpException))
                routeData.Values.Add("action", "Index");
            else
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        routeData.Values.Add("action", "NotFound");
                        break;

                    case 500:
                        routeData.Values.Add("action", "InternalError");
                        break;

                    default:
                        routeData.Values.Add("action", "Index");
                        break;
                }

            routeData.Values.Add("error", exception);
            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;

            IController errorController = new ErrorController();
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}