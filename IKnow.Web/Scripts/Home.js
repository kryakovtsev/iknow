﻿"use strict";

var snapScroll = $("section").SnapScroll({
    hashes: false,
    scrollDelay: 50,
    wheelInterval: 1000,
    animateDuration: 1000,
    animateTimeBuffer: 800,
    maxWheelDeviation: 1000
});

function hideOrShowNav() {
    if ($(document).scrollTop() !== 0)
        $(navbar).fadeOut("fast");
    else
        $(navbar).fadeIn("fast");
}

$(navbar).hide();
hideOrShowNav();

$(document).scroll(hideOrShowNav);

const showCount = (() => {
    var done = false;
    return () => {
        const addToCounter = () => {
            if (parseInt($(testCounter).text()) < parseInt($(testCounter).attr("count"))) {
                $(testCounter).text(parseInt($(testCounter).text()) + 4);

                setTimeout(addToCounter, 10);
            }
            if (parseInt($(testCounter).text()) >= parseInt($(testCounter).attr("count")))
                $(testCounter).text($(testCounter).attr("count"));
        };

        if (!done) {
            done = true;

            setTimeout(addToCounter, 50);
        }
    };
})();

var observer = new IntersectionObserver(function(entries) {
        if (entries[0].isIntersecting === true)
            showCount();
    },
    { threshold: [1] });

observer.observe(countSection);

$(document).ready(function() {
    $(this).scrollTop(0);
});