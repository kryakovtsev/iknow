﻿$(".loader").fadeOut("slow");

function toggleDropdown(e) {
    const _d = $(e.target).closest(".dropdown"),
        _m = $(".dropdown-menu", _d);
    setTimeout(function() {
            const shouldOpen = e.type !== "click" && _d.is(":hover");
            _m.toggleClass("show", shouldOpen);
            _d.toggleClass("show", shouldOpen);
            $('[data-toggle="dropdown"]', _d).attr("aria-expanded", shouldOpen);
        },
        e.type === "mouseleave" ? 300 : 0);
}

$("body")
    .on("mouseenter mouseleave", ".dropdown", toggleDropdown)
    .on("click", ".dropdown-menu a", toggleDropdown);


var forms = document.querySelectorAll("form");

var validation = Array.prototype.filter.call(forms,
    function(form) {
        form.addEventListener("submit",
            function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add("was-validated");
            },
            false);
    });