﻿namespace IKnow.DataAccess.Entities
{
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    public class Category : IDeletableModel
    {
        [Key] public long Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
    }
}