﻿namespace IKnow.DataAccess.Entities
{
    using Infrastructure;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUserRole : IdentityRole,
                                       IDeletableModel
    {
        public ApplicationUserRole()
        {
        }

        public ApplicationUserRole(string name) : base(name)
        {
        }

        public bool Deleted { get; set; }
    }
}