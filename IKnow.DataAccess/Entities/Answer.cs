﻿namespace IKnow.DataAccess.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class Answer : IDeletableModel
    {
        [Key] public long Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }

        public long? QuestionId { get; set; }
        [ForeignKey("QuestionId")] public virtual Question Question { get; set; }

        public bool Deleted { get; set; }
    }
}