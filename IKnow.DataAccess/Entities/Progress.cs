﻿namespace IKnow.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class Progress : IDeletableModel
    {
        public long Id { get; set; }
        public bool Finished { get; set; }
        public DateTime StartTime { get; set; }

        public long? ProfileId { get; set; }
        [ForeignKey("ProfileId")] public virtual UserProfile Profile { get; set; }

        public long? TestId { get; set; }
        [ForeignKey("TestId")] public virtual Test Test { get; set; }

        public virtual List<AnsweredQuestion> Answered { get; set; } = new List<AnsweredQuestion>();

        public bool Deleted { get; set; }
    }
}