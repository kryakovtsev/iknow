﻿namespace IKnow.DataAccess.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class Question : IDeletableModel
    {
        [Key] public long Id { get; set; }
        public string Text { get; set; }

        public virtual long? TestId { get; set; }
        [ForeignKey("TestId")] public virtual Test Test { get; set; }

        public virtual List<Answer> Answers { get; set; } = new List<Answer>();

        public bool Deleted { get; set; }
    }
}