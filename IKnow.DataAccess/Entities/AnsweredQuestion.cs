﻿namespace IKnow.DataAccess.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class AnsweredQuestion : IDeletableModel
    {
        public long Id { get; set; }

        public long? AnswerId { get; set; }
        [ForeignKey("AnswerId")] public virtual Answer Answer { get; set; }

        public long? QuestionId { get; set; }
        [ForeignKey("QuestionId")] public virtual Question Question { get; set; }

        public long? ProgressId { get; set; }
        [ForeignKey("ProgressId")] public virtual Progress Progress { get; set; }
        public bool Deleted { get; set; }
    }
}