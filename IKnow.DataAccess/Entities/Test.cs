﻿namespace IKnow.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class Test : IDeletableModel
    {
        [Key] public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TimeLimit { get; set; }
        public string HexBackground { get; set; } = "#5e5e73";
        public DateTime CreationTime { get; set; } = DateTime.Now;
        public DateTime EditTime { get; set; } = DateTime.Now;
        public int Views { get; set; }

        public virtual long? ImageId { get; set; }
        [ForeignKey("ImageId")] public virtual Image Image { get; set; }

        public virtual long? CategoryId { get; set; }
        [ForeignKey("CategoryId")] public virtual Category Category { get; set; }

        public virtual List<Question> Questions { get; set; } = new List<Question>();

        public bool Deleted { get; set; }
    }
}