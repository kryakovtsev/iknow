﻿namespace IKnow.DataAccess.Entities
{
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    public class Image : IDeletableModel
    {
        [Key] public long Id { get; set; }
        [Required] public string Path { get; set; }
        public bool Deleted { get; set; }
    }
}