﻿namespace IKnow.DataAccess.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infrastructure;

    public class UserProfile : IDeletableModel
    {
        [Key] public long Id { get; set; }
        public DateTime RegisterTime { get; set; }

        public long? ImageId { get; set; }
        [ForeignKey("ImageId")] public virtual Image Image { get; set; }

        public string ApplicationUserId { get; set; }

        [ForeignKey("ApplicationUserId")] public virtual ApplicationUser ApplicationUser { get; set; }

        public bool Deleted { get; set; }
    }
}