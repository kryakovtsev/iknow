﻿namespace IKnow.DataAccess.Entities
{
    using Infrastructure;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUser : IdentityUser, IDeletableModel
    {
        public long? UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public bool Deleted { get; set; }
    }
}