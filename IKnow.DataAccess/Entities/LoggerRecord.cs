﻿namespace IKnow.DataAccess.Entities
{
    using Infrastructure;

    public class LoggerRecord : IDeletableModel
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Level { get; set; }
        public bool Deleted { get; set; }
    }
}