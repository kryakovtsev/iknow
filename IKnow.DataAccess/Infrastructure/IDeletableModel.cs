﻿namespace IKnow.DataAccess.Infrastructure
{
    public interface IDeletableModel
    {
        bool Deleted { get; set; }
    }
}