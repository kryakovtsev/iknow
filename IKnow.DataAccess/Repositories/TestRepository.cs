﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class TestRepository : IRepository<Test>
    {
        private readonly DatabaseContext _database;

        public TestRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Test> Included => _database.Tests.Include(test => test.Category)
                                                      .Include(test => test.Questions)
                                                      .Include(test => test.Questions
                                                                           .Select(p => p.Answers))
                                                      .Include(test => test.Image);

        private IQueryable<Test> NotDeletedTests => Included.Where(test => !test.Deleted);

        /// <summary>
        /// Get all <see cref="Test"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Test> GetAll()
        {
            return NotDeletedTests;
        }

        /// <summary>
        /// Get <see cref="Test"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Test Get(long id)
        {
            return NotDeletedTests.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="Test"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Test> GetAsync(long id)
        {
            return await NotDeletedTests.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Test"/> that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Test> Find(Func<Test, bool> predicate)
        {
            return NotDeletedTests.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="Test"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Test item)
        {
            _database.Set<Test>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Test"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Test item)
        {
            _database.Set<Test>().AddOrUpdate(item);
        }

        /// <summary>
        /// Soft delete <see cref="Test"/> by id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Tests.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}