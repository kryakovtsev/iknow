﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class UserRepository : IRepository<UserProfile>
    {
        private readonly DatabaseContext _db;

        public UserRepository(DatabaseContext context)
        {
            _db = context;
        }

        private IQueryable<UserProfile> NotDeletedUsers => _db.UserProfiles.Include(user => user.ApplicationUser)
                                                              .Include(user => user.Image).Where(user => !user.Deleted);

        /// <summary>
        ///     Get all not deleted user profiles from database
        /// </summary>
        /// <returns>List of user profiles</returns>
        public IEnumerable<UserProfile> GetAll()
        {
            return NotDeletedUsers;
        }

        /// <summary>
        ///     Get user profile by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserProfile Get(long id)
        {
            return NotDeletedUsers.FirstOrDefault(user => user.Id == id);
        }

        /// <summary>
        ///     Get user profile by id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<UserProfile> GetAsync(long id)
        {
            return await NotDeletedUsers.FirstOrDefaultAsync(user => user.Id == id);
        }

        /// <summary>
        ///     Get list of users where <paramref name="predicate" /> is true
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate)
        {
            return NotDeletedUsers.Where(predicate).ToList();
        }

        /// <summary>
        ///     Insert user profile instance to database
        /// </summary>
        /// <param name="item"></param>
        public void Create(UserProfile item)
        {
            _db.UserProfiles.Add(item);
        }

        /// <summary>
        ///     Set instance state to <see cref="EntityState.Modified" /> in <see cref="DatabaseContext" />
        /// </summary>
        /// <param name="item"></param>
        public void Update(UserProfile item)
        {
            _db.Set<UserProfile>().AddOrUpdate(item);
            var applicationUser = _db.Users.FirstOrDefault(u => u.Id == item.ApplicationUserId);
            if (applicationUser == null) return;
            
            applicationUser.Email = item.ApplicationUser.Email;
            applicationUser.UserName = item.ApplicationUser.Email;
            _db.Set<IdentityUser>().AddOrUpdate(applicationUser);
        }

        /// <summary>
        ///     Phantom delete of user instance if it exists in database
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _db.UserProfiles.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}