﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class ProgressRepository : IRepository<Progress>
    {
        private readonly DatabaseContext _database;

        public ProgressRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Progress> Included => _database.Progresses.Include(test => test.Profile)
                                                          .Include(test => test.Answered)
                                                          .Include(test => test.Test);

        private IQueryable<Progress> NotDeletedTests => Included.Where(p => !p.Deleted);

        /// <summary>
        /// Get all <see cref="Progress"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Progress> GetAll()
        {
            return NotDeletedTests;
        }

        /// <summary>
        /// Get <see cref="Progress"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Progress Get(long id)
        {
            return NotDeletedTests.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="Progress"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Progress> GetAsync(long id)
        {
            return await NotDeletedTests.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Progress"/> that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Progress> Find(Func<Progress, bool> predicate)
        {
            return NotDeletedTests.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="Progress"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Progress item)
        {
            _database.Set<Progress>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Progress"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Progress item)
        {
            _database.Set<Progress>().AddOrUpdate(item);
        }

        /// <summary>
        /// Soft delete <see cref="Progress"/> by id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Progresses.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}