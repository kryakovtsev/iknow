﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class AnswerRepository : IRepository<Answer>
    {
        private readonly DatabaseContext _database;

        public AnswerRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Answer> Included => _database.Answers.Include(a => a.Question);
        private IQueryable<Answer> NotDeletedCategories => Included.Where(test => !test.Deleted);

        /// <summary>
        /// Get all <see cref="Answer"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Answer> GetAll()
        {
            return NotDeletedCategories;
        }

        /// <summary>
        /// Get <see cref="Answer"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Answer Get(long id)
        {
            return NotDeletedCategories.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="Answer"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Answer> GetAsync(long id)
        {
            return await NotDeletedCategories.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Answer"/> that match predicate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Answer> Find(Func<Answer, bool> predicate)
        {
            return NotDeletedCategories.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="Answer"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Answer item)
        {
            _database.Set<Answer>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Answer"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Answer item)
        {
            _database.Set<Answer>().AddOrUpdate(item);
        }

        /// <summary>
        /// Soft delete <see cref="Answer"/> by id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Answers.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}