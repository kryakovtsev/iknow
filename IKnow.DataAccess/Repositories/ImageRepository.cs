﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class ImageRepository : IRepository<Image>
    {
        private readonly DatabaseContext _database;

        public ImageRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Image> AllImages => _database.Images.Where(m => !m.Deleted);

        /// <summary>
        /// Get all <see cref="Image"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Image> GetAll()
        {
            return AllImages;
        }

        /// <summary>
        /// Get <see cref="Image"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Image Get(long id)
        {
            return AllImages.FirstOrDefault(img => img.Id == id);
        }

        /// <summary>
        /// Get <see cref="Image"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Image> GetAsync(long id)
        {
            return await AllImages.FirstOrDefaultAsync(img => img.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Image"/> that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Image> Find(Func<Image, bool> predicate)
        {
            return AllImages.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="Image"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Image item)
        {
            _database.Set<Image>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Image"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Image item)
        {
            _database.Set<Image>().AddOrUpdate(item);
        }

        /// <summary>
        /// Throws <see cref="NotSupportedException"/>
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Images.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}