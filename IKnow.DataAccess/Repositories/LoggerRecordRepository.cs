﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class LoggerRecordRepository : IRepository<LoggerRecord>
    {
        private readonly DatabaseContext _database;

        public LoggerRecordRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<LoggerRecord> AllRecords => _database.LoggerRecords.Where(m => !m.Deleted);

        public IEnumerable<LoggerRecord> GetAll()
        {
            return AllRecords;
        }

        /// <summary>
        /// Get <see cref="LoggerRecord"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LoggerRecord Get(long id)
        {
            return AllRecords.FirstOrDefault(record => record.Id == id);
        }

        /// <summary>
        /// Get <see cref="LoggerRecord"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<LoggerRecord> GetAsync(long id)
        {
            return await AllRecords.FirstOrDefaultAsync(record => record.Id == id);
        }

        /// <summary>
        /// Find <see cref="LoggerRecord"/> by predicate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<LoggerRecord> Find(Func<LoggerRecord, bool> predicate)
        {
            return AllRecords.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="LoggerRecord"/> 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Create(LoggerRecord item)
        {
            _database.Set<LoggerRecord>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="LoggerRecord"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(LoggerRecord item)
        {
            _database.Set<LoggerRecord>().AddOrUpdate(item);
        }

        /// <summary>
        /// Throws <see cref="NotSupportedException"/>
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.LoggerRecords.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}