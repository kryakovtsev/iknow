﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class AnsweredQuestionRepository : IRepository<AnsweredQuestion>
    {
        private readonly DatabaseContext _database;

        public AnsweredQuestionRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<AnsweredQuestion> Included => _database
                                                         .AnsweredQuestions
                                                         .Include(answeredQuestion => answeredQuestion.Answer)
                                                         .Include(answeredQuestion => answeredQuestion.Progress)
                                                         .Include(answeredQuestion => answeredQuestion.Question);

        private IQueryable<AnsweredQuestion> NotDeletedTests => Included.Where(aq => !aq.Deleted);

        /// <summary>
        /// Get all <see cref="AnsweredQuestion"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AnsweredQuestion> GetAll()
        {
            return NotDeletedTests;
        }

        /// <summary>
        /// Get <see cref="AnsweredQuestion"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AnsweredQuestion Get(long id)
        {
            return NotDeletedTests.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="AnsweredQuestion"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AnsweredQuestion> GetAsync(long id)
        {
            return await NotDeletedTests.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Find all <see cref="AnsweredQuestion"/> that match predicate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<AnsweredQuestion> Find(Func<AnsweredQuestion, bool> predicate)
        {
            return NotDeletedTests.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="AnsweredQuestion"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(AnsweredQuestion item)
        {
            _database.Set<AnsweredQuestion>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="AnsweredQuestion"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(AnsweredQuestion item)
        {
            _database.Set<AnsweredQuestion>().AddOrUpdate(item);
        }

        /// <summary>
        /// Throws <see cref="NotSupportedException"/>
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.AnsweredQuestions.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}