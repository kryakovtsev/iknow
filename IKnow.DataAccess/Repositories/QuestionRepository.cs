﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class QuestionRepository : IRepository<Question>
    {
        private readonly DatabaseContext _database;

        public QuestionRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Question> Included => _database.Questions;
        private IQueryable<Question> NotDeletedCategories => Included.Where(test => !test.Deleted);

        /// <summary>
        /// Get all <see cref="Question"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Question> GetAll()
        {
            return NotDeletedCategories;
        }

        /// <summary>
        /// Get <see cref="Question"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Question Get(long id)
        {
            return NotDeletedCategories.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="Question"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Question> GetAsync(long id)
        {
            return await NotDeletedCategories.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Question"/> that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Question> Find(Func<Question, bool> predicate)
        {
            return NotDeletedCategories.Where(predicate);
        }

        /// <summary>
        /// Create <see cref="Question"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Question item)
        {
            _database.Set<Question>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Queryable"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Question item)
        {
            _database.Set<Question>().AddOrUpdate(item);
        }

        /// <summary>
        /// Soft delete <see cref="Question"/>
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Questions.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}