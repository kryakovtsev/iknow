﻿namespace IKnow.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;
    using DataContext;
    using Entities;

    public class CategoryRepository : IRepository<Category>
    {
        private readonly DatabaseContext _database;

        public CategoryRepository(DatabaseContext database)
        {
            _database = database;
        }

        private IQueryable<Category> Included => _database.Categories;
        private IQueryable<Category> NotDeletedCategories => Included.Where(test => !test.Deleted);

        /// <summary>
        /// Get all <see cref="Category"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Category> GetAll()
        {
            return NotDeletedCategories;
        }

        /// <summary>
        /// Get <see cref="Category"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category Get(long id)
        {
            return NotDeletedCategories.FirstOrDefault(test => test.Id == id);
        }

        /// <summary>
        /// Get <see cref="Category"/> by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Category> GetAsync(long id)
        {
            return await NotDeletedCategories.FirstOrDefaultAsync(test => test.Id == id);
        }

        /// <summary>
        /// Get all <see cref="Category"/> that match predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<Category> Find(Func<Category, bool> predicate)
        {
            return NotDeletedCategories.Where(predicate);
        }

        /// <summary>
        /// Create new <see cref="Category"/>
        /// </summary>
        /// <param name="item"></param>
        public void Create(Category item)
        {
            _database.Set<Category>().AddOrUpdate(item);
        }

        /// <summary>
        /// Update <see cref="Category"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(Category item)
        {
            _database.Set<Category>().AddOrUpdate(item);
        }

        /// <summary>
        /// Soft delete <see cref="Category"/>
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            var model = _database.Categories.Find(id);
            if (model != null)
                model.Deleted = true;
        }
    }
}