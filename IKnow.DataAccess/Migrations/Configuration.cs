﻿namespace IKnow.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DataContext;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DatabaseContext context)
        {
            if (context.Set<IdentityRole>().FirstOrDefault(role => role.Name == "admin") == null)
                context.Roles.Add(new IdentityRole {Name = "admin"});

            if (context.Roles.FirstOrDefault(role => role.Name == "user") == null)
                context.Roles.Add(new IdentityRole {Name = "user"});

            context.SaveChanges();
        }
    }
}