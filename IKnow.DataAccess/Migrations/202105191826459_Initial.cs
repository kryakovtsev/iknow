﻿namespace IKnow.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                        "dbo.AnsweredQuestions",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 QuestionId = c.Long(),
                                 QuestionAnswerId = c.Long(),
                                 ProgressId = c.Long()
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestProgresses", t => t.ProgressId)
                .ForeignKey("dbo.TestQuestions", t => t.QuestionId)
                .ForeignKey("dbo.TestQuestionAnswers", t => t.QuestionAnswerId)
                .Index(t => t.QuestionId)
                .Index(t => t.QuestionAnswerId)
                .Index(t => t.ProgressId);

            CreateTable(
                        "dbo.TestProgresses",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 ProfileId = c.Long(),
                                 TestId = c.Long(),
                                 Finished = c.Boolean(false),
                                 StartTime = c.DateTime(false),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.ProfileId)
                .ForeignKey("dbo.TestInformations", t => t.TestId)
                .Index(t => t.ProfileId)
                .Index(t => t.TestId);

            CreateTable(
                        "dbo.UserProfiles",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 ImageId = c.Long(),
                                 RegisterTime = c.DateTime(false),
                                 ApplicationUserId = c.String(maxLength: 128),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .Index(t => t.ImageId)
                .Index(t => t.ApplicationUserId);

            CreateTable(
                        "dbo.AspNetUsers",
                        c => new
                             {
                                 Id = c.String(false, 128),
                                 Email = c.String(maxLength: 256),
                                 EmailConfirmed = c.Boolean(false),
                                 PasswordHash = c.String(),
                                 SecurityStamp = c.String(),
                                 PhoneNumber = c.String(),
                                 PhoneNumberConfirmed = c.Boolean(false),
                                 TwoFactorEnabled = c.Boolean(false),
                                 LockoutEndDateUtc = c.DateTime(),
                                 LockoutEnabled = c.Boolean(false),
                                 AccessFailedCount = c.Int(false),
                                 UserName = c.String(false, 256),
                                 UserProfileId = c.Long(),
                                 Deleted = c.Boolean(),
                                 Discriminator = c.String(false, 128)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.UserProfileId);

            CreateTable(
                        "dbo.AspNetUserClaims",
                        c => new
                             {
                                 Id = c.Int(false, true),
                                 UserId = c.String(false, 128),
                                 ClaimType = c.String(),
                                 ClaimValue = c.String()
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId);

            CreateTable(
                        "dbo.AspNetUserLogins",
                        c => new
                             {
                                 LoginProvider = c.String(false, 128),
                                 ProviderKey = c.String(false, 128),
                                 UserId = c.String(false, 128)
                             })
                .PrimaryKey(t => new {t.LoginProvider, t.ProviderKey, t.UserId})
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId);

            CreateTable(
                        "dbo.AspNetUserRoles",
                        c => new
                             {
                                 UserId = c.String(false, 128),
                                 RoleId = c.String(false, 128)
                             })
                .PrimaryKey(t => new {t.UserId, t.RoleId})
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                        "dbo.Images",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Path = c.String(false)
                             })
                .PrimaryKey(t => t.Id);

            CreateTable(
                        "dbo.TestInformations",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Name = c.String(),
                                 Description = c.String(),
                                 TimeLimit = c.Int(false),
                                 HexBackground = c.String(),
                                 CreationTime = c.DateTime(false),
                                 EditTime = c.DateTime(false),
                                 Views = c.Int(false),
                                 ImageId = c.Long(),
                                 CategoryId = c.Long(),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .ForeignKey("dbo.TestCategories", t => t.CategoryId)
                .Index(t => t.ImageId)
                .Index(t => t.CategoryId);

            CreateTable(
                        "dbo.TestQuestions",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Text = c.String(),
                                 TestInformationId = c.Long(),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestInformations", t => t.TestInformationId)
                .Index(t => t.TestInformationId);

            CreateTable(
                        "dbo.TestQuestionAnswers",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Text = c.String(),
                                 IsCorrect = c.Boolean(false),
                                 TestQuestionId = c.Long(),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestQuestions", t => t.TestQuestionId)
                .Index(t => t.TestQuestionId);

            CreateTable(
                        "dbo.TestCategories",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Name = c.String(),
                                 Deleted = c.Boolean(false)
                             })
                .PrimaryKey(t => t.Id);

            CreateTable(
                        "dbo.AspNetRoles",
                        c => new
                             {
                                 Id = c.String(false, 128),
                                 Name = c.String(false, 256),
                                 Discriminator = c.String(false, 128)
                             })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
        }

        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AnsweredQuestions", "AnswerId", "dbo.TestQuestionAnswers");
            DropForeignKey("dbo.AnsweredQuestions", "QuestionId", "dbo.TestQuestions");
            DropForeignKey("dbo.TestProgresses", "TestId", "dbo.TestInformations");
            DropForeignKey("dbo.TestInformations", "CategoryId", "dbo.TestCategories");
            DropForeignKey("dbo.TestQuestions", "TestId", "dbo.TestInformations");
            DropForeignKey("dbo.TestQuestionAnswers", "QuestionId", "dbo.TestQuestions");
            DropForeignKey("dbo.TestInformations", "ImageId", "dbo.Images");
            DropForeignKey("dbo.TestProgresses", "ProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "ImageId", "dbo.Images");
            DropForeignKey("dbo.UserProfiles", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.AnsweredQuestions", "ProgressId", "dbo.TestProgresses");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.TestQuestionAnswers", new[] {"QuestionId"});
            DropIndex("dbo.TestQuestions", new[] {"TestId"});
            DropIndex("dbo.TestInformations", new[] {"CategoryId"});
            DropIndex("dbo.TestInformations", new[] {"ImageId"});
            DropIndex("dbo.AspNetUserRoles", new[] {"RoleId"});
            DropIndex("dbo.AspNetUserRoles", new[] {"UserId"});
            DropIndex("dbo.AspNetUserLogins", new[] {"UserId"});
            DropIndex("dbo.AspNetUserClaims", new[] {"UserId"});
            DropIndex("dbo.AspNetUsers", new[] {"UserProfileId"});
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.UserProfiles", new[] {"ApplicationUserId"});
            DropIndex("dbo.UserProfiles", new[] {"ImageId"});
            DropIndex("dbo.TestProgresses", new[] {"TestId"});
            DropIndex("dbo.TestProgresses", new[] {"ProfileId"});
            DropIndex("dbo.AnsweredQuestions", new[] {"ProgressId"});
            DropIndex("dbo.AnsweredQuestions", new[] {"AnswerId"});
            DropIndex("dbo.AnsweredQuestions", new[] {"QuestionId"});
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TestCategories");
            DropTable("dbo.TestQuestionAnswers");
            DropTable("dbo.TestQuestions");
            DropTable("dbo.TestInformations");
            DropTable("dbo.Images");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.TestProgresses");
            DropTable("dbo.AnsweredQuestions");
        }
    }
}