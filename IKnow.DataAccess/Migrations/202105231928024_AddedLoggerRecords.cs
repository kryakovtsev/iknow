﻿namespace IKnow.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedLoggerRecords : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                        "dbo.LoggerRecords",
                        c => new
                             {
                                 Id = c.Long(false, true),
                                 Message = c.String(),
                                 StackTrace = c.String(),
                                 Level = c.String()
                             })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.LoggerRecords");
        }
    }
}