﻿namespace IKnow.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FixedImageInUserProfile : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.AnsweredQuestions", "QuestionAnswerId", "AnswerId");
            RenameColumn("dbo.Answers", "TestQuestionId", "QuestionId");
            RenameColumn("dbo.Questions", "TestInformationId", "TestId");
            RenameIndex("dbo.AnsweredQuestions", "IX_QuestionAnswerId", "IX_AnswerId");
            RenameIndex("dbo.Answers", "IX_TestQuestionId", "IX_QuestionId");
            RenameIndex("dbo.Questions", "IX_TestInformationId", "IX_TestId");
        }

        public override void Down()
        {
            RenameIndex("dbo.Questions", "IX_TestId", "IX_TestInformationId");
            RenameIndex("dbo.Answers", "IX_QuestionId", "IX_TestQuestionId");
            RenameIndex("dbo.AnsweredQuestions", "IX_AnswerId", "IX_QuestionAnswerId");
            RenameColumn("dbo.Questions", "TestId", "TestInformationId");
            RenameColumn("dbo.Answers", "QuestionId", "TestQuestionId");
            RenameColumn("dbo.AnsweredQuestions", "AnswerId", "QuestionAnswerId");
        }
    }
}