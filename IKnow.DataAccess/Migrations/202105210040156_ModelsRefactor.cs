﻿namespace IKnow.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ModelsRefactor : DbMigration
    {
        public override void Up()
        {
            RenameTable("dbo.TestProgresses", "Progresses");
            RenameTable("dbo.TestInformations", "Tests");
            RenameTable("dbo.TestQuestions", "Questions");
            RenameTable("dbo.TestQuestionAnswers", "Answers");
            RenameTable("dbo.TestCategories", "Categories");
        }

        public override void Down()
        {
            RenameTable("dbo.Categories", "TestCategories");
            RenameTable("dbo.Answers", "TestQuestionAnswers");
            RenameTable("dbo.Questions", "TestQuestions");
            RenameTable("dbo.Tests", "TestInformations");
            RenameTable("dbo.Progresses", "TestProgresses");
        }
    }
}