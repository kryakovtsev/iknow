﻿namespace IKnow.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletableModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnsweredQuestions", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Images", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.LoggerRecords", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetRoles", "Deleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "Deleted");
            DropColumn("dbo.LoggerRecords", "Deleted");
            DropColumn("dbo.Images", "Deleted");
            DropColumn("dbo.AnsweredQuestions", "Deleted");
        }
    }
}
