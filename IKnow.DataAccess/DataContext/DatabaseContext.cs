﻿namespace IKnow.DataAccess.DataContext
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using Entities;
    using Microsoft.AspNet.Identity.EntityFramework;

    public sealed class DatabaseContext : IdentityDbContext
    {
        public DatabaseContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Progress> Progresses { get; set; }
        public DbSet<AnsweredQuestion> AnsweredQuestions { get; set; }
        public DbSet<LoggerRecord> LoggerRecords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>()
                        .HasOptional(x => x.UserProfile)
                        .WithMany()
                        .HasForeignKey(x => x.UserProfileId);

            modelBuilder.Entity<UserProfile>()
                        .HasOptional(x => x.ApplicationUser)
                        .WithMany()
                        .HasForeignKey(x => x.ApplicationUserId);

            base.OnModelCreating(modelBuilder);
        }
    }

    public class MigrationsContextFactory : IDbContextFactory<DatabaseContext>
    {
        public DatabaseContext Create()
        {
            return new DatabaseContext("DefaultConnection");
        }
    }
}