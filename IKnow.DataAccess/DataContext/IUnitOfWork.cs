﻿namespace IKnow.DataAccess.DataContext
{
    using System;
    using System.Threading.Tasks;
    using Entities;
    using Identity;
    using Repositories;

    public interface IUnitOfWork : IDisposable
    {
        IRepository<UserProfile> Users { get; }
        IRepository<Test> Tests { get; }
        IRepository<Category> Categories { get; }
        IRepository<Image> Images { get; }
        IRepository<Question> Questions { get; }
        IRepository<Answer> Answers { get; }
        IRepository<Progress> Progresses { get; }
        IRepository<AnsweredQuestion> AnsweredQuestions { get; }
        IRepository<LoggerRecord> Logger { get; }

        ApplicationUserManager IdentityUserManager { get; }
        ApplicationRoleManager RoleManager { get; }

        Task SaveAsync();
        void Save();
    }
}