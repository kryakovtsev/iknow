﻿namespace IKnow.DataAccess.DataContext
{
    using System;
    using System.Threading.Tasks;
    using Entities;
    using Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Repositories;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _db;
        private AnsweredQuestionRepository _answeredQuestionsRepository;
        private AnswerRepository _answersRepository;
        private ApplicationRoleManager _applicationRoleManager;
        private ApplicationUserManager _applicationUserManager;

        private bool _disposed;
        private ImageRepository _imagesRepository;
        private LoggerRecordRepository _logger;
        private UserRepository _profileRepository;
        private ProgressRepository _progressesRepository;
        private QuestionRepository _questionsRepository;
        private CategoryRepository _testCategoriesRepository;
        private TestRepository _testRepository;

        public UnitOfWork(string connectionString)
        {
            _db = new DatabaseContext(connectionString);
        }

        public IRepository<UserProfile> Users => _profileRepository ??= new UserRepository(_db);
        public IRepository<Test> Tests => _testRepository ??= new TestRepository(_db);
        public IRepository<Category> Categories => _testCategoriesRepository ??= new CategoryRepository(_db);
        public IRepository<Image> Images => _imagesRepository ??= new ImageRepository(_db);
        public IRepository<Question> Questions => _questionsRepository ??= new QuestionRepository(_db);

        public IRepository<Answer> Answers =>
            _answersRepository ??= new AnswerRepository(_db);

        public IRepository<Progress> Progresses =>
            _progressesRepository ??= new ProgressRepository(_db);

        public IRepository<AnsweredQuestion> AnsweredQuestions =>
            _answeredQuestionsRepository ??= new AnsweredQuestionRepository(_db);

        public IRepository<LoggerRecord> Logger =>
            _logger ??= new LoggerRecordRepository(_db);

        public ApplicationUserManager IdentityUserManager =>
            _applicationUserManager ??= new ApplicationUserManager(new UserStore<ApplicationUser>(_db));

        public ApplicationRoleManager RoleManager => _applicationRoleManager ??=
                                                         new ApplicationRoleManager(new RoleStore<ApplicationUserRole
                                                         >(_db));

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            _disposed = true;

            if (!disposing) return;

            Save();
            _db.Dispose();
        }
    }
}