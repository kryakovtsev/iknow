﻿namespace IKnow.DataAccess.Identity
{
    using Entities;
    using Microsoft.AspNet.Identity;

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
            PasswordValidator = new PasswordValidator
                                {
                                    RequiredLength = 8,
                                    RequireNonLetterOrDigit = false,
                                    RequireDigit = false,
                                    RequireLowercase = false,
                                    RequireUppercase = false
                                };
        }
    }
}