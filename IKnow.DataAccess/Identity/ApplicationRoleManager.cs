﻿namespace IKnow.DataAccess.Identity
{
    using Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationRoleManager : RoleManager<ApplicationUserRole>
    {
        public ApplicationRoleManager(RoleStore<ApplicationUserRole> store)
            : base(store)
        {
        }
    }
}