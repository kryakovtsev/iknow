﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace IKnow.Tests
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using BusinessLogic.DataTransferObjects;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Services;
    using Moq;
    using Web.Controllers;

    [TestClass]
    public class HomeControllerTest
    {
        private Mock<IUserService> _userService;

        [TestInitialize]
        public void SetUp()
        {
            _userService = new Mock<IUserService>(); 
        }

        [TestMethod]
        public async Task IndexNoLoginViewResultNotNull()
        {
            _userService.Setup(m => m.GetByEmailAsync(It.IsAny<string>()))
                        .Returns<UserDto>(total => Task.FromResult<UserDto>(null));
            HomeController controller = new HomeController(_userService.Object);

            ViewResult result = await controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task IndexLoginShouldRedirect()
        {
            _userService.Setup(m => m.GetByEmailAsync(It.IsAny<string>()))
                        .Returns<UserDto>(total => Task.FromResult<UserDto>(new UserDto()
                                                                            {
                                                                                Email = "admin@gmail.com",
                                                                                Name = "admin@gmail.com",
                                                                                Role = "admin"
                                                                            }));
            HomeController controller = new HomeController(_userService.Object);

            RedirectToRouteResult result = await controller.Index() as RedirectToRouteResult;

            Assert.IsNotNull(result);
        }
    }
}
